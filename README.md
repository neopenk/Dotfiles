# Dotfiles

These are the config files for my Plasma Desktop, Latte Dock, Konsole, and Doom Emacs. The intended way to install these configs is by using Konsave (available in the AUR). To apply these configs, you must clone this repository and put this profile in your ~/.config/konsave/profiles folder, and then run the command konsave -a boundless-dark to apply the profile. Beware that all of your KDE and Doom configs will be overwritten. To prevent this, you should at least save a profile of your own that you can fall back to if you don't like mine.

It should also be noted that installing this doesn't install any packages for you. For these configs to work as intended, you need to install Latte Dock, Kvantum, Lightly, Yakuake, the Reversal-grey icons, the Layan Plasma Theme, the Whitesur Aurorae, the Meslo Monospace Font (ttf-meslo-nerd-font-powerlevel10k from the AUR), Font Awesome 5, and a few Plasma Applets, such as the virtual desktop bar (plasma5-applets-virtual-desktop-bar-git from the AUR), better inline clock, window appmenu (plasma5-applets-window-appmenu from the AUR), window title applet (plasma5-applets-window-title from the AUR), latte sidebar button, window buttons applet (plasma5-applets-window-buttons-git from the AUR), kpple menu, present windows button, latte spacer, latte separator, media controller plus, configurable button, and ditto menu.

If you want window tiling to work, you also need to install the Bismuth Kwin script (kwin-bismuth on the aur)

Note: I made this with a 1366x768 resolution, so if you try to use this in 1080p some UI items and fonts may be a little small. This however shouldn't be difficult to fix.
