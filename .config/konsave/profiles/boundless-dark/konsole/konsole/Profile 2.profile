[Appearance]
ColorScheme=DoomOne
Font=Meslo LG M DZ for Powerline,9,-1,5,50,0,0,0,0,0

[Cursor Options]
UseCustomCursorColor=false

[General]
Command=zsh
Name=Profile 2
Parent=FALLBACK/

[Interaction Options]
OpenLinksByDirectClickEnabled=true
UnderlineFilesEnabled=true

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
