[Appearance]
AntiAliasFonts=true
ColorScheme=DoomOne
Font=Meslo LG M for Powerline,9,-1,5,50,0,0,0,0,0
UseFontLineChararacters=true

[General]
Command=zsh
Name=Profile 4
Parent=FALLBACK/

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=2
HistorySize=200000
ScrollBarPosition=1

[Terminal Features]
BlinkingCursorEnabled=false
